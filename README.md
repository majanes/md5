These files were cloned from Apitrace's thirdparty directory here:
  https://gitlab.freedesktop.org/majanes/apitrace/-/tree/master/thirdparty/md5

FrameRetrace Meson build support leveraged this subproject within
Apitrace, but since 0.56, sub-subprojects report an error.
Specifically:

```ERROR: Sandbox violation: Tried to grab file md5.c from a different
subproject.```

was generated starting with:

```
  c659be692896f9f36fa46c4955220dc57653f09b
  Author:     Xavier Claessens <xavier.claessens@collabora.com>
  Interpreter: Fix nested subsubproject detection
```


Work around this issue by putting the md5 lib in it's own git wrap.